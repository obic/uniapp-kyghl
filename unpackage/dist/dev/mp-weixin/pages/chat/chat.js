(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["pages/chat/chat"],{

/***/ 328:
/*!******************************************************************!*\
  !*** F:/obic/uniapp-obic/main.js?{"page":"pages%2Fchat%2Fchat"} ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(wx, createPage) {

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ 4);
__webpack_require__(/*! uni-pages */ 26);
var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 25));
var _chat = _interopRequireDefault(__webpack_require__(/*! ./pages/chat/chat.vue */ 329));
// @ts-ignore
wx.__webpack_require_UNI_MP_PLUGIN__ = __webpack_require__;
createPage(_chat.default);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/wx.js */ 1)["default"], __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["createPage"]))

/***/ }),

/***/ 329:
/*!***********************************************!*\
  !*** F:/obic/uniapp-obic/pages/chat/chat.vue ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./chat.vue?vue&type=template&id=bf16e7f4&scoped=true& */ 330);
/* harmony import */ var _chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./chat.vue?vue&type=script&lang=js& */ 332);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _chat_vue_vue_type_style_index_0_id_bf16e7f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./chat.vue?vue&type=style&index=0&id=bf16e7f4&lang=scss&scoped=true& */ 334);
/* harmony import */ var _D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js */ 32);

var renderjs





/* normalize component */

var component = Object(_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "bf16e7f4",
  null,
  false,
  _chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["components"],
  renderjs
)

component.options.__file = "pages/chat/chat.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 330:
/*!******************************************************************************************!*\
  !*** F:/obic/uniapp-obic/pages/chat/chat.vue?vue&type=template&id=bf16e7f4&scoped=true& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns, recyclableRender, components */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--17-0!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/template.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-uni-app-loader/page-meta.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./chat.vue?vue&type=template&id=bf16e7f4&scoped=true& */ 331);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "recyclableRender", function() { return _D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["recyclableRender"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "components", function() { return _D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_17_0_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_template_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_uni_app_loader_page_meta_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_template_id_bf16e7f4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["components"]; });



/***/ }),

/***/ 331:
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--17-0!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/template.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-uni-app-loader/page-meta.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!F:/obic/uniapp-obic/pages/chat/chat.vue?vue&type=template&id=bf16e7f4&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns, recyclableRender, components */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recyclableRender", function() { return recyclableRender; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "components", function() { return components; });
var components
try {
  components = {
    ktNavBar: function () {
      return __webpack_require__.e(/*! import() | uni_modules/uniapp-kantboot/components/kt-nav-bar/kt-nav-bar */ "uni_modules/uniapp-kantboot/components/kt-nav-bar/kt-nav-bar").then(__webpack_require__.bind(null, /*! @/uni_modules/uniapp-kantboot/components/kt-nav-bar/kt-nav-bar.vue */ 530))
    },
    uLoadmore: function () {
      return Promise.all(/*! import() | uni_modules/uview-ui/components/u-loadmore/u-loadmore */[__webpack_require__.e("common/vendor"), __webpack_require__.e("uni_modules/uview-ui/components/u-loadmore/u-loadmore")]).then(__webpack_require__.bind(null, /*! @/uni_modules/uview-ui/components/u-loadmore/u-loadmore.vue */ 522))
    },
    goodsCardRequest: function () {
      return Promise.all(/*! import() | components/goods-card-request/goods-card-request */[__webpack_require__.e("common/vendor"), __webpack_require__.e("components/goods-card-request/goods-card-request")]).then(__webpack_require__.bind(null, /*! @/components/goods-card-request/goods-card-request.vue */ 537))
    },
    uGrid: function () {
      return Promise.all(/*! import() | uni_modules/uview-ui/components/u-grid/u-grid */[__webpack_require__.e("common/vendor"), __webpack_require__.e("uni_modules/uview-ui/components/u-grid/u-grid")]).then(__webpack_require__.bind(null, /*! @/uni_modules/uview-ui/components/u-grid/u-grid.vue */ 430))
    },
    uGridItem: function () {
      return Promise.all(/*! import() | uni_modules/uview-ui/components/u-grid-item/u-grid-item */[__webpack_require__.e("common/vendor"), __webpack_require__.e("uni_modules/uview-ui/components/u-grid-item/u-grid-item")]).then(__webpack_require__.bind(null, /*! @/uni_modules/uview-ui/components/u-grid-item/u-grid-item.vue */ 438))
    },
    ktKeyboardSeize: function () {
      return __webpack_require__.e(/*! import() | uni_modules/uniapp-kantboot/components/kt-keyboard-seize/kt-keyboard-seize */ "uni_modules/uniapp-kantboot/components/kt-keyboard-seize/kt-keyboard-seize").then(__webpack_require__.bind(null, /*! @/uni_modules/uniapp-kantboot/components/kt-keyboard-seize/kt-keyboard-seize.vue */ 350))
    },
  }
} catch (e) {
  if (
    e.message.indexOf("Cannot find module") !== -1 &&
    e.message.indexOf(".vue") !== -1
  ) {
    console.error(e.message)
    console.error("1. 排查组件名称拼写是否正确")
    console.error(
      "2. 排查组件是否符合 easycom 规范，文档：https://uniapp.dcloud.net.cn/collocation/pages?id=easycom"
    )
    console.error(
      "3. 若组件不符合 easycom 规范，需手动引入，并在 components 中注册该组件"
    )
  } else {
    throw e
  }
}
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  var g0 = _vm.$i18n.zhToGlobal("聊天")
  var g1 = !_vm.isInit
    ? _vm.$kt.file.byPath("tabbar/message-selected.svg")
    : null
  var g2 = _vm.loading ? _vm.$i18n.zhToGlobal("加载中") : null
  var l0 = _vm.__map(_vm.list, function (item, index) {
    var $orig = _vm.__get_orig(item)
    var m0 = _vm.isSelf(item.userAccountId)
    var g3 =
      m0 && item.typeCode === "fileIdOfImg"
        ? _vm.$kt.file.visit(item.content)
        : null
    var g4 =
      m0 && item.typeCode === "fileIdOfVideo"
        ? _vm.$kt.file.visit(item.content)
        : null
    var g5 = m0
      ? _vm.$kt.date.format(item.gmtCreate, "yyyy-MM-dd hh:mm:ss")
      : null
    var g6 = m0 ? _vm.$kt.userAccount.getSelf() : null
    var g7 =
      m0 && g6.fileIdOfAvatar
        ? _vm.$kt.file.visit(_vm.$kt.userAccount.getSelf().fileIdOfAvatar)
        : null
    var g8 =
      m0 && !g6.fileIdOfAvatar ? _vm.$kt.file.byPath("image/logo.png") : null
    var g9 =
      !m0 && _vm.userInfo.fileIdOfAvatar
        ? _vm.$kt.file.visit(_vm.userInfo.fileIdOfAvatar)
        : null
    var g10 =
      !m0 && !_vm.userInfo.fileIdOfAvatar
        ? _vm.$kt.file.byPath("image/logo.png")
        : null
    var g11 =
      !m0 && item.typeCode === "fileIdOfImg"
        ? _vm.$kt.file.visit(item.content)
        : null
    var g12 =
      !m0 && item.typeCode === "fileIdOfVideo"
        ? _vm.$kt.file.visit(item.content)
        : null
    var g13 = !m0
      ? _vm.$kt.date.format(item.gmtCreate, "yyyy-MM-dd hh:mm:ss")
      : null
    return {
      $orig: $orig,
      m0: m0,
      g3: g3,
      g4: g4,
      g5: g5,
      g6: g6,
      g7: g7,
      g8: g8,
      g9: g9,
      g10: g10,
      g11: g11,
      g12: g12,
      g13: g13,
    }
  })
  var g14 = _vm.$i18n.zhToGlobal("请输入")
  var g15 = this.requestParam.value
    ? _vm.$kt.file.byPath("image/plan.svg")
    : null
  var g16 = _vm.$kt.file.byPath("image/plan.svg")
  var g17 = _vm.$i18n.zhToGlobal("商品")
  var g18 = _vm.$i18n.zhToGlobal("图片")
  var g19 = _vm.$i18n.zhToGlobal("视频")
  if (!_vm._isMounted) {
    _vm.e0 = function ($event, item) {
      var _temp = arguments[arguments.length - 1].currentTarget.dataset,
        _temp2 = _temp.eventParams || _temp["event-params"],
        item = _temp2.item
      var _temp, _temp2
      _vm.previewImage(_vm.$kt.file.visit(item.content))
    }
    _vm.e1 = function ($event, item) {
      var _temp3 = arguments[arguments.length - 1].currentTarget.dataset,
        _temp4 = _temp3.eventParams || _temp3["event-params"],
        item = _temp4.item
      var _temp3, _temp4
      _vm.previewImage(_vm.$kt.file.visit(item.content))
    }
  }
  _vm.$mp.data = Object.assign(
    {},
    {
      $root: {
        g0: g0,
        g1: g1,
        g2: g2,
        l0: l0,
        g14: g14,
        g15: g15,
        g16: g16,
        g17: g17,
        g18: g18,
        g19: g19,
      },
    }
  )
}
var recyclableRender = false
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ 332:
/*!************************************************************************!*\
  !*** F:/obic/uniapp-obic/pages/chat/chat.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/babel-loader/lib!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--13-1!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/script.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./chat.vue?vue&type=script&lang=js& */ 333);
/* harmony import */ var _D_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_D_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _D_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _D_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_D_HBuilderX_plugins_uniapp_cli_node_modules_babel_loader_lib_index_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_13_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_script_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 333:
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--13-1!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/script.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!F:/obic/uniapp-obic/pages/chat/chat.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var UserCommonInfoCard = function UserCommonInfoCard() {
  __webpack_require__.e(/*! require.ensure | components/user-info/user-common-info-card */ "components/user-info/user-common-info-card").then((function () {
    return resolve(__webpack_require__(/*! @/components/user-info/user-common-info-card.vue */ 544));
  }).bind(null, __webpack_require__)).catch(__webpack_require__.oe);
};
var _default = {
  components: {
    UserCommonInfoCard: UserCommonInfoCard
  },
  data: function data() {
    return {
      scrollIntoViewId: '',
      headerHeight: 0,
      footerHeight: 0,
      chatDialog: {},
      userInfo: {},
      requestParam: {
        value: ''
      },
      isInit: false,
      list: [],
      keyboardHeight: 0,
      loading: false,
      goodsMap: {}
    };
  },
  // 监听requestParam.value的变化
  watch: {
    "requestParam.value": function requestParamValue(val) {
      this.getHeight();
    }
  },
  onShow: function onShow() {
    var _this = this;
    this.getHeight();
    // 获取页面参数
    var currentPages = getCurrentPages();
    var option = currentPages[currentPages.length - 1].options;
    console.debug(option.dialogId, "option");
    this.getById(option.dialogId);
    this.chatDialog.id = option.dialogId;
    this.$forceUpdate();
    uni.$on("goods:choose", function (goods) {
      if (!goods) {
        return;
      }
      _this.$kt.request.send({
        uri: '/bus-chat-web/content/sendContent',
        data: {
          dialogId: option.dialogId,
          typeCode: 'goodsId',
          content: goods.id
        },
        stateSuccess: function stateSuccess(res) {
          console.debug(res, "success");
          _this.getAfter(option.dialogId);
          setTimeout(function () {
            _this.moveToEnd();
          }, 500);
          setTimeout(function () {
            _this.moveToEnd();
          }, 1000);
        },
        stateFail: function stateFail(res) {
          console.debug(res, "faild");
        }
      });
    });
    // 记录上次的contentId
    uni.$on("chat:message:new", function (res) {
      console.log("测试", option.dialogId);
      console.log(option.dialogId, "option.dialogId");

      // 获取当前页面的路径
      var currentPagePath = getCurrentPages();
      currentPagePath = currentPagePath[currentPagePath.length - 1].route;
      if (currentPagePath !== 'pages/chat/chat') {
        uni.$off("chat:message:new");
        return;
      }
      _this.getAfter(option.dialogId);
    });
  },
  methods: {
    chooseGoods: function chooseGoods() {
      uni.navigateTo({
        url: '/pages/goodsSearch/goodsSearch?isChoose=1'
      });
    },
    // 移动到最后
    moveToEnd: function moveToEnd() {
      var _this2 = this;
      if (this.loading) {
        return;
      }
      this.scrollIntoViewId = "";
      setTimeout(function () {
        _this2.scrollIntoViewId = 'scrollIntoView';
      }, 100);
    },
    // 打开预览图片
    previewImage: function previewImage(url) {
      uni.previewImage({
        current: url,
        urls: [url]
      });
    },
    isSelf: function isSelf(id) {
      return this.$kt.userAccount.getSelf().id === id;
    },
    getList: function getList(dialogId) {
      var _this3 = this;
      this.$kt.request.send({
        uri: '/bus-chat-web/content/getList',
        data: {
          dialogId: dialogId
        },
        stateSuccess: function stateSuccess(res) {
          // console.log(JSON.stringify(res.data));
          var resData = res.data;
          if (!resData) {
            resData = [];
          }
          _this3.list = resData;
          _this3.moveToEnd();
          uni.$emit("chat:new");
        },
        stateFail: function stateFail(res) {}
      });
    },
    getAfter: function getAfter(dialogId) {
      var _this4 = this;
      console.debug(dialogId, "dialogId-getAfter");
      // if(!dialogId){
      //   return;
      // }
      var minId = 0;
      if (this.list.length > 0) {
        minId = this.list[this.list.length - 1].id;
      }
      this.$kt.request.send({
        uri: '/bus-chat-web/content/getAfter',
        data: {
          dialogId: this.chatDialog.id,
          minId: minId
        },
        stateSuccess: function stateSuccess(res) {
          console.log(JSON.stringify(res.data), "---");
          uni.$emit("chat:new");
          for (var i = 0; i < res.data.length; i++) {
            // this.list.push(res.data[i]);
            // 如果list没有值，则直接push
            if (_this4.list.length === 0) {
              _this4.list.push(res.data[i]);
              _this4.moveToEnd();
            } else {
              // 如果list有值，则判断是否重复，重复则不添加，不重复则添加
              var isRepeat = false;
              for (var j = 0; j < _this4.list.length; j++) {
                if (_this4.list[j].id === res.data[i].id) {
                  isRepeat = true;
                  break;
                }
              }
              if (!isRepeat) {
                _this4.list.push(res.data[i]);
                _this4.moveToEnd();
              }
            }
          }
        },
        stateFail: function stateFail(res) {}
      });
    },
    getBefore: function getBefore(dialogId) {
      var _this5 = this;
      if (!dialogId) {
        return;
      }
      if (this.loading) {
        return;
      }
      this.loading = true;
      this.$kt.request.send({
        uri: '/bus-chat-web/content/getBefore',
        data: {
          dialogId: dialogId,
          maxId: this.list[0].id
        },
        stateSuccess: function stateSuccess(res) {
          // 倒序
          for (var i = res.data.length - 1; i >= 0; i--) {
            // 如果list没有值，则直接push
            if (_this5.list.length === 0) {
              _this5.list.push(res.data[i]);
              _this5.moveToEnd();
            } else {
              // 如果list有值，则判断是否重复，重复则不添加，不重复则添加
              var isRepeat = false;
              for (var j = 0; j < _this5.list.length; j++) {
                if (_this5.list[j].id === res.data[i].id) {
                  isRepeat = true;
                  break;
                }
              }
              if (!isRepeat) {
                // 往前加
                _this5.list.unshift(res.data[i]);
              }
            }
          }
          _this5.scrollIntoViewId = 'scrollIntoViewTop';
          setTimeout(function () {
            _this5.scrollIntoViewId = '';
            _this5.loading = false;
          }, 100);
        },
        stateFail: function stateFail(res) {
          _this5.loading = false;
        }
      });
    },
    sendContent: function sendContent() {
      var _this6 = this;
      if (!this.requestParam.value) {
        return;
      }
      var value = this.requestParam.value + "";
      this.requestParam.value = "";
      this.$kt.request.send({
        uri: '/bus-chat-web/content/sendContent',
        data: {
          dialogId: this.chatDialog.id,
          typeCode: 'text',
          content: value
        },
        stateSuccess: function stateSuccess(res) {
          console.debug(res, "success");
          _this6.$kt.event.emit('chat:sendSuccess', {
            dialogId: _this6.chatDialog.id,
            typeCode: 'text',
            content: value
          });
          _this6.getAfter(_this6.chatDialog.id);
        },
        stateFail: function stateFail(res) {
          console.debug(res, "faild");
        }
      });
    },
    // 选择图片
    chooseImage: function chooseImage() {
      var _this7 = this;
      uni.chooseImage({
        count: 1,
        sizeType: ['original'],
        sourceType: ['album', 'camera'],
        success: function success(res) {
          console.log(res);
          var filePath = res.tempFilePaths[0];
          uni.showLoading({
            title: _this7.$i18n.zhToGlobal('上传中')
          });
          _this7.$kt.request.uploadFile({
            uri: '/bus-chat-web/content/sendContent',
            name: 'file',
            data: {
              file: filePath,
              groupCode: 'chat'
            },
            stateSuccess: function stateSuccess(res) {
              _this7.$kt.request.send({
                uri: '/bus-chat-web/content/sendContent',
                data: {
                  dialogId: _this7.chatDialog.id,
                  typeCode: 'fileIdOfImg',
                  content: res.data.id
                },
                stateSuccess: function stateSuccess(res) {
                  console.log(res);
                  uni.hideLoading();
                  _this7.getAfter(_this7.chatDialog.id);
                  _this7.$kt.event.emit('chat:sendSuccess', {
                    dialogId: _this7.chatDialog.id,
                    typeCode: 'fileOfImg',
                    content: res.data.id
                  });
                },
                stateFail: function stateFail(res) {}
              });
            },
            stateFail: function stateFail(res) {}
          });
        }
      });
    },
    // 选择视频
    chooseVideo: function chooseVideo() {
      var _this8 = this;
      // 不压缩
      uni.chooseVideo({
        compressed: false,
        sourceType: ['album', 'camera'],
        maxDuration: 60,
        camera: 'back',
        success: function success(res) {
          console.log(res);
          var filePath = res.tempFilePath;
          uni.showLoading({
            title: _this8.$i18n.zhToGlobal('上传中'),
            // 时间最多3秒
            duration: 3000
          });
          _this8.$kt.request.uploadFile({
            uri: '/bus-chat-web/content/sendContent',
            name: 'file',
            data: {
              file: filePath,
              groupCode: 'chat'
            },
            stateSuccess: function stateSuccess(res) {
              _this8.$kt.request.send({
                uri: '/bus-chat-web/content/sendContent',
                data: {
                  dialogId: _this8.chatDialog.id,
                  typeCode: 'fileIdOfVideo',
                  content: res.data.id
                },
                stateSuccess: function stateSuccess(res) {
                  _this8.$kt.event.emit('chat:sendSuccess', {
                    dialogId: _this8.chatDialog.id,
                    typeCode: 'fileIdOfVideo',
                    content: res.data.id
                  });
                  uni.hideLoading();
                  console.debug(res);
                  _this8.getAfter(_this8.chatDialog.id);
                },
                stateFail: function stateFail(res) {
                  uni.hideLoading();
                }
              });
            },
            stateFail: function stateFail(res) {}
          });
        }
      });
    },
    getHeight: function getHeight() {
      var _this9 = this;
      // 获取底部高度
      uni.createSelectorQuery().select('#footerInChatPage').boundingClientRect(function (data) {
        _this9.footerHeight = data.height;
        console.debug(_this9.footerHeight, "this.footerHeight");
      }).exec();

      // 获取头部高度
      uni.createSelectorQuery().select('#headerInChatPage').boundingClientRect(function (data) {
        _this9.headerHeight = data.height;
        console.debug(_this9.headerHeight, "this.headerHeight");
      }).exec();
      setTimeout(function () {
        // 获取底部高度
        uni.createSelectorQuery().select('#footerInChatPage').boundingClientRect(function (data) {
          _this9.footerHeight = data.height;
          console.debug(_this9.footerHeight, "this.footerHeight");
        }).exec();

        // 获取头部高度
        uni.createSelectorQuery().select('#headerInChatPage').boundingClientRect(function (data) {
          _this9.headerHeight = data.height;
          console.debug(_this9.headerHeight, "this.headerHeight");
        }).exec();
      }, 50);
      setTimeout(function () {
        // 获取底部高度
        uni.createSelectorQuery().select('#footerInChatPage').boundingClientRect(function (data) {
          _this9.footerHeight = data.height;
          console.debug(_this9.footerHeight, "this.footerHeight");
        }).exec();

        // 获取头部高度
        uni.createSelectorQuery().select('#headerInChatPage').boundingClientRect(function (data) {
          _this9.headerHeight = data.height;
          console.debug(_this9.headerHeight, "this.headerHeight");
        }).exec();
      }, 100);
      setTimeout(function () {
        // 获取底部高度
        uni.createSelectorQuery().select('#footerInChatPage').boundingClientRect(function (data) {
          _this9.footerHeight = data.height;
          console.debug(_this9.footerHeight, "this.footerHeight");
        }).exec();

        // 获取头部高度
        uni.createSelectorQuery().select('#headerInChatPage').boundingClientRect(function (data) {
          _this9.headerHeight = data.height;
          console.debug(_this9.headerHeight, "this.headerHeight");
        }).exec();
      }, 500);
      setTimeout(function () {
        // 获取底部高度
        uni.createSelectorQuery().select('#footerInChatPage').boundingClientRect(function (data) {
          _this9.footerHeight = data.height;
          console.debug(_this9.footerHeight, "this.footerHeight");
        }).exec();

        // 获取头部高度
        uni.createSelectorQuery().select('#headerInChatPage').boundingClientRect(function (data) {
          _this9.headerHeight = data.height;
          console.debug(_this9.headerHeight, "this.headerHeight");
        }).exec();
      }, 1000);
    },
    getById: function getById(id) {
      var _this10 = this;
      this.$kt.request.send({
        uri: '/bus-chat-web/dialog/getById',
        data: {
          id: id
        },
        stateSuccess: function stateSuccess(res) {
          _this10.chatDialog = res.data;
          console.debug(JSON.stringify(res.data));
          var userAccountIds = res.data.userAccountIds.split(",");
          // 获取自身ID
          var selfId = _this10.$kt.userAccount.getSelf().id;
          var otherId = userAccountIds[0] + "" === selfId + "" ? userAccountIds[1] : userAccountIds[0];
          _this10.getList(_this10.chatDialog.id);
          _this10.getUserById(otherId);
        },
        stateFail: function stateFail(res) {}
      });
    },
    // /user-account-web/userAccount/getById
    getUserById: function getUserById(id) {
      var _this11 = this;
      this.$kt.request.send({
        uri: '/user-account-web/userAccount/getById',
        data: {
          id: id
        },
        stateSuccess: function stateSuccess(res) {
          console.log(JSON.stringify(res.data));
          _this11.userInfo = res.data;
          setTimeout(function () {
            _this11.isInit = true;
          }, 500);
        },
        stateFail: function stateFail(res) {
          uni.showToast({
            title: res.errMsg,
            icon: 'none'
          });
        }
      });
    }
  }
};
exports.default = _default;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 2)["default"]))

/***/ }),

/***/ 334:
/*!*********************************************************************************************************!*\
  !*** F:/obic/uniapp-obic/pages/chat/chat.vue?vue&type=style&index=0&id=bf16e7f4&lang=scss&scoped=true& ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _D_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_D_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_2_D_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_5_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_style_index_0_id_bf16e7f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!./node_modules/mini-css-extract-plugin/dist/loader.js??ref--8-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--8-oneOf-1-1!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--8-oneOf-1-2!./node_modules/postcss-loader/src??ref--8-oneOf-1-3!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/sass-loader/dist/cjs.js??ref--8-oneOf-1-4!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--8-oneOf-1-5!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!./chat.vue?vue&type=style&index=0&id=bf16e7f4&lang=scss&scoped=true& */ 335);
/* harmony import */ var _D_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_D_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_2_D_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_5_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_style_index_0_id_bf16e7f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_D_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_D_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_2_D_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_5_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_style_index_0_id_bf16e7f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _D_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_D_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_2_D_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_5_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_style_index_0_id_bf16e7f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _D_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_D_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_2_D_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_5_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_style_index_0_id_bf16e7f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_D_HBuilderX_plugins_uniapp_cli_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_D_HBuilderX_plugins_uniapp_cli_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_loaders_stylePostLoader_js_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_2_D_HBuilderX_plugins_uniapp_cli_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_3_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_sass_loader_dist_cjs_js_ref_8_oneOf_1_4_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_webpack_preprocess_loader_index_js_ref_8_oneOf_1_5_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_vue_cli_plugin_uni_packages_vue_loader_lib_index_js_vue_loader_options_D_HBuilderX_plugins_uniapp_cli_node_modules_dcloudio_webpack_uni_mp_loader_lib_style_js_chat_vue_vue_type_style_index_0_id_bf16e7f4_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ 335:
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js??ref--8-oneOf-1-0!./node_modules/css-loader/dist/cjs.js??ref--8-oneOf-1-1!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--8-oneOf-1-2!./node_modules/postcss-loader/src??ref--8-oneOf-1-3!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/sass-loader/dist/cjs.js??ref--8-oneOf-1-4!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/webpack-preprocess-loader??ref--8-oneOf-1-5!./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib??vue-loader-options!./node_modules/@dcloudio/webpack-uni-mp-loader/lib/style.js!F:/obic/uniapp-obic/pages/chat/chat.vue?vue&type=style&index=0&id=bf16e7f4&lang=scss&scoped=true& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin
    if(false) { var cssReload; }
  

/***/ })

},[[328,"common/runtime","common/vendor"]]]);
//# sourceMappingURL=../../../.sourcemap/mp-weixin/pages/chat/chat.js.map