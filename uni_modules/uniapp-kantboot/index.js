import config from "@/uni_modules/uniapp-kantboot/libs/config";
import file from "@/uni_modules/uniapp-kantboot/libs/file";
import util from "@/uni_modules/uniapp-kantboot/libs/util";
import crypto from "@/uni_modules/uniapp-kantboot/libs/crypto";
import router from "@/uni_modules/uniapp-kantboot/libs/router";
import storage from "@/uni_modules/uniapp-kantboot/libs/storage";
import event from "@/uni_modules/uniapp-kantboot/libs/event";
import request from "@/uni_modules/uniapp-kantboot/libs/request";
import requestSecurity from "@/uni_modules/uniapp-kantboot/libs/requestSecurity";
import image from "@/uni_modules/uniapp-kantboot/libs/image";
import global from "@/uni_modules/uniapp-kantboot/libs/global";
import date from "@/uni_modules/uniapp-kantboot/libs/date";
import i18n from "@/uni_modules/uniapp-kantboot/libs/i18n";
import userAccount from "@/uni_modules/uniapp-kantboot/libs/userAccount";
import math from "@/uni_modules/uniapp-kantboot/libs/math";
import websocket from "@/uni_modules/uniapp-kantboot/libs/websocket";

event.init();

setInterval(() => {
    event.queueHandle();
}, 100);

export default {
    config,
    file,
    crypto,
    util,
    router,
    storage,
    event,
    request,
    requestSecurity,
    image,
    date,
    global,
    i18n,
    userAccount,
    math,
    websocket
}