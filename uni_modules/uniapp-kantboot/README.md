# Kantboot在Uniapp的js工具

## 介绍
uniapp-kantboot工具集是我在多个项目实践中逐步封装和完善的一套UniApp专用JS工具类，旨在为开发者提供简洁、高效的开发体验。它包含了一系列常用的工具方法，涵盖了日期处理、通用操作、本地存储、图片裁剪、事件管理和高精度运算等功能。

## 安装方式
在main.js中引入
```javascript
import kantboot from '@/uni_modules/uniapp-kantboot';

// 挂载到Vue原型上
Vue.prototype.$kt = kantboot;
```

### 工具

#### 时间处理工具(Date)

```javascript
/*
 * 转换为常用格式
 * 时间戳转换为YY-MM-DD hh:mm:ss格式
 */
// 可时间戳、Date对象、通用时间格式字符串
$kt.date.toCommonFormat(984700800000);
```

```javascript
/*
 * 格式转换
 */
// 可时间戳、Date对象、通用时间格式字符串
$kt.date.format(984700800000, "yyyy年MM月dd日hh点mm分ss秒");
```

```javascript
/*
 * 可读模式
 * 比如 刚刚、3分钟前、1小时前、昨天 12:00、2020-12-12 12:00
 * 注：此方法还未进行国际化、只支持中文、会在后续版本进行国际化
 */
// 可时间戳、Date对象、通用时间格式字符串
$kt.date.toReadable(984700800000);
```

```javascript
/*
 * 根据出生日期计算年龄
 */
// 可时间戳、Date对象、通用时间格式字符串
$kt.date.getAge(984700800000);

/*
 * 如不采用本机当前时间，可自行传入时间
 */
let assumingNow = 1607731200000;
$kt.date.getAge(984700800000, assumingNow);
```


#### 通用工具(Util)
```javascript
/*
 * 生成UUID
 * 默认长度为32位
 */
$kt.util.generateUUID();

/*
 * 生成UUID（自定义长度） 
 */
$kt.util.generateUUID(64);
```

```javascript
/*
 * 判断内容是否为空
 * 用来判断字符串、数组、对象是否为空（不包含0，0不属于空）
 */
$kt.util.isEmpty(''); // true
$kt.util.isEmpty([]); // true
$kt.util.isEmpty({}); // true
$kt.util.isEmpty(0); // false
```

```javascript
/*
 * px转rpx
 */
$kt.util.pxToRpx(100);
```

```javascript
/*
 * rpx转px
 */
$kt.util.rpxToPx(100);
```

```javascript
/*
 * 给json按照key的字母顺序排序
 * 不改变原json,返回排序后的json
 */
let json = {b: 1, a: 2, c: 3};
let newJson = $kt.util.sortJson(json); // {a: 2, b: 1, c: 3}
```

```javascript
/*
 * 获取距离的可读格式
 * 参数单位为米
 */
$kt.util.readableDistance(1000); // {value: 1, unit: 'km'}

// 直读模式
$kt.util.readableDistanceReadMode(1000); // 1km
```

```javascript
/*
 * 获取存储大小的可读格式
 * 参数单位为字节
 */
$kt.util.readableStorage(1024); // {value: 1, unit: 'KB'}

// 直读模式
$kt.util.readableStorageReadMode(1024); // 1KB
```

```javascript
/*
 * 对象深拷贝
 * 深拷贝的对象将不和原对象使用同一内存地址
 */
let obj = {a: 1, b: {c: 2}};
let newObj = $kt.util.deepCopy(obj);
```


#### 本地存储(Storage)
```javascript
/*
 * 设置本地存储
 */
$kt.storage.set("key", "value");

// 设置过期时间（单位：毫秒）
$kt.storage.setEx("key", "value", 1000);
```

```javascript
/*
 * 获取本地存储
 */
$kt.storage.get("key");
```

```javascript
/*
 * 删除本地存储
 */
$kt.storage.remove("key");
```

```javascript
/*
 *存储锁
 */
// 如不解锁默认时间为1秒
if($kt.storage.lock("lockKey")) {
    console.log("锁定成功");
    // 解锁
    $kt.storage.unlock("lockKey");
}
```

```javascript
/*
 * 根据key前缀删除本地存储
 */
$kt.storage.removeByPrefix("key");
```
更多请看<a href="./libs/storage/index.js">源码</a>

#### 图片处理(Image)
```javascript
/*
 * 图片裁剪
 * 参数1：文件对象
 * 参数2：裁剪宽度
 * 参数3：裁剪高度
 */
// file为文件对象
$kt.image.toImageClip(file,100,100).then((tempFilePath)=>{
    // tempFilePath为裁剪后的图片路径
    console.log(tempFilePath);
});

// 需要在pages.json中配置路径
/*
{
    "path": "uni_modules/uniapp-kantboot/pages/imageCropper/imageCropper",
    "style": {
        "navigationBarTitleText": "",
        "enablePullDownRefresh": false
    }
}
 */
```


#### 事件（Event）
```javascript
/*
 * 事件触发（全局，横跨页面）
 */
$kt.event.emit("eventName");

// 带参数
$kt.event.emit("eventName", "参数1", {a: 1});
```

```javascript
/*
 * 事件监听（全局，横跨页面）
 */
$kt.event.on("eventName", ()=> {
    console.log("事件触发");
});

// 带参数
$kt.event.on("eventName", (arg1, arg2)=> {
    console.log("事件触发", arg1, arg2);
});
```

```javascript
/*
 * 移除事件监听
 */
$kt.event.off("eventName");
```

#### 不丢失精度运算（Math）
```javascript
/*
 * 运算
 * 参数1：表达式（字符串）
 */
$kt.math.evaluate("1+2*3*(0.1+0.2)");
```

```javascript
/*
 * 加法
 */
$kt.math.add(0.1, 0.2);

// 减法 sub，乘法 mul，除法 div
```

```javascript
/*
 * 批量加法
 */
$kt.math.batchAdd(0.1, 0.2, 0.3);

// 批量减法 batchSub，批量乘法 batchMul，批量除法 batchDiv
```

#### 网络请求（Request）
```javascript
this.$kt.request.send({
    uri: "/user-account-web/userAccountLogin/loginByUsernameAndPassword",
    // 检测到未登录时是否跳转到登录页，默认为true
    isJumpLogin: true,
    // 是否断网重连，默认为false
    isReconnect: true,
    data: {
        username: "admin",
        password: "123456"
    },
    stateSuccess: (res) => {
        console.log(res);
    },
    stateFail: (res) => {
        console.log(res);
    }
});

// 如需加密请求，将$kt.request.send替换为$kt.requestSecurity.send，将前后端双向加密
this.$kt.requestSecurity.send({
    uri: "/user-account-web/userAccountLogin/loginByUsernameAndPassword",
    // 检测到未登录时是否跳转到登录页，默认为true
    isJumpLogin: true,
    // 是否断网重连，默认为false
    isReconnect: true,
    data: {
        username: "admin",
        password: "123456"
    },
    stateSuccess: (res) => {
        console.log(res);
    },
    stateFail: (res) => {
        console.log(res);
    }
});
```
仅做示例，还没有做一个独立于前端的请求封装，现在需要和Kantboot后端一起用。