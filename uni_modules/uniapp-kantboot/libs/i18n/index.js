import request from "@/uni_modules/uniapp-kantboot/libs/request";
import storage from "@/uni_modules/uniapp-kantboot/libs/storage";
import util from "@/uni_modules/uniapp-kantboot/libs/util";

let result = {};

/**
 * 获取当前语言编码
 * @returns {*} 语言编码
 */
result.getLanguageCode = function () {
    if (storage.get("languageCode")) {
        return storage.get("languageCode");
    }
    return uni.getSystemInfoSync().osLanguage
        || uni.getSystemInfoSync().language
        || uni.getSystemInfoSync().hostLanguage.split("-")[0]
        || "en-US";
}

/**
 * 设置当前语言编码
 * @param languageCode 语言编码
 */
result.setLanguageCode = function (languageCode) {
    // 是否存在语言本地化，如果存在就设置成语言本地化中的语言编码
    let dictI18nLocalizedMap = storage.get("dictI18nLocalizedMap");
    if (!dictI18nLocalizedMap) {
        storage.set("languageCode", languageCode);
        return;
    }
    if (dictI18nLocalizedMap[languageCode]) {
        storage.set("languageCode", dictI18nLocalizedMap[languageCode]);
        return;
    }
    storage.set("languageCode", languageCode);
}

// getLocalizedList
/**
 * 获取语言本地化
 */
result.loadingLocalized = function () {
    return new Promise((resolve, reject) => {
        request.send({
            uri: "/system-dict-web/language/getLocalizedList",
            stateSuccess: (res) => {
                console.debug("获取语言本地化成功", res.data);
                // 将语言本地化存储到本地
                storage.set("dictI18nLocalizedList", res.data);
                // 转换为map存储
                let dictI18nLocalizedMap = {};
                res.data.forEach((item) => {
                    dictI18nLocalizedMap[item.code] = item.languageCode;
                });
                storage.set("dictI18nLocalizedMap", dictI18nLocalizedMap);
                result.setLanguageCode(result.getLanguageCode());
                resolve(res.data);
            },
            stateFail: (res) => {
                uni.showToast({
                    title: "获取语言本地化错误：" + res.errMsg,
                    icon: "none"
                })
                reject();
            }
        });
    });
}

/**
 * 加载语言包
 */
result.loadLanguage = function (dictGroupCode, languageCode) {
    // 如果languageCode为空，则获取当前语言编码
    if (!languageCode) {
        languageCode = result.getLanguageCode();
    }

    return new Promise((resolve, reject) => {
        request.send({
            uri: "/system-dict-web/dictI18n/getDict",
            data: {
                languageCode: languageCode,
                dictGroupCode: dictGroupCode
            },
            stateSuccess: (res) => {
                console.debug("加载语言包成功", res.data);
                // 将语言包存储到本地
                storage.set("dictI18nList_" + dictGroupCode + "_" + languageCode, res.data);
                // 转换为map存储
                let dictI18nMap = {};
                res.data.forEach((item) => {
                    dictI18nMap[item.dictCode] = item.value;
                });
                storage.set("dictI18nMap_" + dictGroupCode + "_" + languageCode, dictI18nMap);

                // 如果是中文，就存储到dictI18nZhToGlobal
                if (languageCode === "zh_CN") {
                    let dictI18nMap2 = {};
                    res.data.forEach((item) => {
                        dictI18nMap2[item.value] = item.dictCode;
                    });
                    storage.set("dictI18nZhToGlobal_" + dictGroupCode, dictI18nMap2);
                }

                resolve(res.data);
            },
            stateFail: (res) => {
                uni.showToast({
                    title: "加载语言包错误：" + res.errMsg,
                    icon: "none"
                })
                reject();
            }
        })
    });
}

result.zhToGlobal = function (value, dictGroupCode) {
    if (!dictGroupCode) {
        dictGroupCode = "appFront";
    }
    try {
        let key = storage.get("dictI18nZhToGlobal_" + dictGroupCode)[value];
        let newVar = storage.get("dictI18nMap_" + dictGroupCode + "_" + result.getLanguageCode())[key];
        return util.firstLetterUpper(newVar);
    } catch (e) {
        return value;
    }
}

result.toGlobal = function (dictCode, dictGroupCode) {
    if (!dictGroupCode) {
        dictGroupCode = "appFront";
    }
    try {
        let value = storage.get("dictI18nMap_" + dictGroupCode + "_" + result.getLanguageCode())[dictCode];
        return util.firstLetterUpper(value);
    } catch (e) {
        return dictCode;
    }
}

export default result;


