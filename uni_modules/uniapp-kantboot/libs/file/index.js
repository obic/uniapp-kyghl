import kt from '../../index.js';

/**
 * 根据文件id访问文件
 */
let visit = function (id) {
    return `${kt.config.requestConfig.fileAddress}/visit/${id}`;
}

/**
 * 根据文件路径访问文件
 * @param {Object} path 文件路径
 * @return string 文件的url
 */
let byPath = function (path) {
    return `${kt.config.requestConfig.staticFileAddress}/${path}`;
}

export default {
    visit,
    byPath,
}
