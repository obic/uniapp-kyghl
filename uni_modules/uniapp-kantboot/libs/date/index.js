let result = {};

/**
 * 转换为常用时间格式
 * 时间戳转换为YY-MM-DD hh:mm:ss格式
 * @param time 时间戳
 * @returns {string} YY-MM-DD hh:mm:ss格式
 */
result.toCommonFormat = function (time) {
    //将时间戳格式转换成年月日时分秒
    let date = new Date(time);
    let Y = date.getFullYear() + '-';
    let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    let D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';

    let h = (date.getHours() < 10 ? '0' + (date.getHours()) : date.getHours()) + ':';
    let m = (date.getMinutes() < 10 ? '0' + (date.getMinutes()) : date.getMinutes()) + ':';
    let s = (date.getSeconds() < 10 ? '0' + (date.getSeconds()) : date.getSeconds());
    return Y + M + D + h + m + s;
};

/**
 * 格式转换
 * @param time
 * @param formatStr
 * @returns {*}
 */
result.format = function (time, formatStr) {
    let date = new Date(parseInt(time));
    let Y = date.getFullYear();
    let M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    let D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());

    let h = (date.getHours() < 10 ? '0' + (date.getHours()) : date.getHours());
    let m = (date.getMinutes() < 10 ? '0' + (date.getMinutes()) : date.getMinutes());
    let s = (date.getSeconds() < 10 ? '0' + (date.getSeconds()) : date.getSeconds());
    return formatStr
        .replace(/[yY]+/, Y)
        .replace(/M+/, M)
        .replace(/[Dd]+/, D)
        .replace(/[h]+/, h)
        .replace(/[m]+/, m)
        .replace(/[s]+/, s);
}

/**
 * 可读模式时间
 * @param {*} dateTime 时间戳
 */
result.toReadable=function(dateTime) {

    // 获取当前时间戳
    let now = new Date().getTime();
    // 如果小于1分钟，显示刚刚
    if (now - dateTime < 60 * 1000) {
        return "刚刚";
    }

    // 如果大于1分钟，小于1小时，显示几分钟前
    if (now - dateTime < 60 * 60 * 1000) {
        // 获取相差的分钟数
        let minutes = Math.floor((now - dateTime) / (60 * 1000));

        return minutes+"分钟前";
    }
    // 获取今天的年月日和dateTime的年月日
    let nowDate = new Date(now);
    let dateTimeDate = new Date(dateTime);
    // 如果是同一天，显示hh:mm
    if (
        nowDate.getFullYear() === dateTimeDate.getFullYear() &&
        nowDate.getMonth() === dateTimeDate.getMonth() &&
        nowDate.getDate() === dateTimeDate.getDate()
    ) {
        return result.format(dateTime, "hh:mm");
    }
    // 如果是昨天，显示昨天 hh:mm
    if (
        nowDate.getFullYear() === dateTimeDate.getFullYear() &&
        nowDate.getMonth() === dateTimeDate.getMonth() &&
        nowDate.getDate() - dateTimeDate.getDate() === 1
    ) {
        return "昨天 " + result.format(dateTime, "hh:mm");
    }
    // 如果是今年，显示MM-dd hh:mm
    if (
        nowDate.getFullYear() === dateTimeDate.getFullYear() &&
        nowDate.getMonth() === dateTimeDate.getMonth()
    ) {
        return result.format(dateTime, "MM-dd hh:mm");
    }
    // 如果是去年，显示yyyy-MM-dd hh:mm
    return result.format(dateTime, "yyyy-MM-dd hh:mm");
}

/**
 * 根据出生日期获取年龄
 * @param {*} gmtBirthday 生日
 * @param {*} assumingNow 设定当前时间
 */
result.getAge = function (gmtBirthday,assumingNow) {
    if(!assumingNow){
        assumingNow = new Date().getTime();
    }
    let now = new Date(assumingNow);
    let birth = new Date(gmtBirthday);
    let age = now.getFullYear() - birth.getFullYear();
    if (now.getMonth() < birth.getMonth() || (now.getMonth() === birth.getMonth() && now.getDate() < birth.getDate())) {
        age--;
    }
    return age;
}



export default result;
