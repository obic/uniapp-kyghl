import requestSecurity from "@/uni_modules/uniapp-kantboot/libs/requestSecurity";
import storage from "@/uni_modules/uniapp-kantboot/libs/storage";
import request from "@/uni_modules/uniapp-kantboot/libs/request";

/**
 * 根据key获取uuid
 * @param key key
 * @returns {Promise<unknown>}
 */
let getUuidByKey = function (key) {
    return new Promise((resolve, reject) => {
        request.send({
            uri: "/globe-data-change-web/getUuidByKey",
            data: {
                key: key
            },
            stateSuccess: (res) => {
                console.debug("获取uuid成功", res.data);
                resolve(res.data);
            },
            stateFail: (res) => {
                uni.showToast({
                    title: "获取uuid错误：" + res.errMsg,
                    icon: "none"
                })
                reject();
            }
        })
    });
}

/**
 * 检查是否改变了数据
 */
let checkDataChange = function (key) {
    let value = storage.get("DataChange:" + key);
    return new Promise((resolve, reject) => {
        getUuidByKey(key).then((res) => {
            // 如果不相等，说明数据改变了
            if (value !== res) {
                // storage.set("DataChange:" + key, res);
                resolve({
                    isChange: true,
                    uuid: res
                });
                return;
            }
            resolve({
                isChange: false,
                uuid: res
            });
        }).catch((err) => {
            console.error("检查数据是否改变错误", err);
            resolve({
                isChange: false,
                uuid: null
            });
        });
    })
}

/**
 * 设置数据改变
 */
let setDataChange = function (key,uuid) {
    storage.set("DataChange:" + key, uuid);
}

export default {
    getUuidByKey,
    checkDataChange,
    setDataChange
}