let result = {};

/**
 * token在存储中的key
 */
result.token = "token";

/**
 * 语言编码在存储中的key
 */
result.languageCode = "languageCode";

/**
 * 服务器公钥在存储中的key
 */
result.serverPublicKey = "serverPublicKey";

/**
 * 客户端密钥
 */
result.clientKey = "clientKey";


export default result;