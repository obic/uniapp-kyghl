// let rootAddress = "http://127.0.0.1:10010";
let rootAddress = "https://obic.aaarfyh.com";

let config={

    // 请求根地址
    rootAddress: rootAddress,

    // 上传地址
    fileUploadAddress: `https://obic.aaarfyh.com/functional-file-web/file/upload`,

    // 文件初始地址
    fileAddress: `https://obic.aaarfyh.com/functional-file-web/file`,

    // 静态文件地址
    staticFileAddress: `https://file.obic.aaarfyh.com/`,

    rootWebSocketAddressList: "ws://127.0.0.1:20002",

    headerField: {
        // 用于验证的令牌字段
        authorization:"token",
    },


    // 响应数据字段
    responseDataField: {
        // 状态码字段
        stateCode: "stateCode",
        // 状态为“成功”时的字段
        stateSuccessMessage: "msg",
        // 状态为“失败”时的字段
        stateFailMessage: "errMsg",
        //主体数据字段
        bodyData: "data",
    },

    // 通用状态码
    stateCode: {
         // 成功状态码，用于配置请求成功的状态码，如：0
        success: "SUCCESS",
        // 失败状态码，用于配置请求失败的状态码，如：-1
        notLogin: "notLogin",
    },

    // 登录页面
    loginPage:"/pages/login/login"

}

export default config;