import keyConfig from "./keyConfig";
import requestConfig from "./requestConfig";
import appConfig from "./appConfig";

let result = {};

/**
 * 关于存储的key的配置
 */
result.keyConfig = keyConfig;

/**
 * 关于url的配置
 */
result.requestConfig = requestConfig;

/**
 * 关于app的配置
 */
result.appConfig = appConfig;

export default result;