import util from "@/uni_modules/uniapp-kantboot/libs/util/index.js";
import router from "@/uni_modules/uniapp-kantboot/libs/router";
import event from "@/uni_modules/uniapp-kantboot/libs/event";

let result = {};

/**
 * 前往图片裁剪
 */
result.toImageClip = (imgSrc,width,height) => {
    // 生成UUID
    let uuid = util.generateUUID(64);
    let params = {
        imgSrc,
        width,
        height,
        uuid
    };
    router.navTo("/uni_modules/uniapp-kantboot/pages/imageCropper/imageCropper",params);
    return new Promise((resolve,reject) => {
        event.on(uuid,(data) => {
            console.log("imageClip",data);
            resolve(data);
        });
    });
}

export default result;

