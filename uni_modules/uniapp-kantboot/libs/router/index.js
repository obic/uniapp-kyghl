let result = {};

/**
 * 获取完整路径
 * @param path 页面路径
 * @param params 参数
 * @return 完整路径
 */
result.getFullPath = function (path,params) {
    if(!path){
        throw new Error("path(第1个参数)不能为空");
    }
    params = params || {};
    let paramStr = "";
    for(let key in params) {
        if (params[key]) {
            paramStr += key + "=" + encodeURIComponent(params[key]) + "&";
        }
    }
    paramStr = paramStr.substring(0, paramStr.length - 1);
    // 路径拼接
    let fullPath = path+"";
    // 判断path中是否有参数
    if (fullPath.indexOf("?") === -1) {
        fullPath += "?";
    } else {
        fullPath += "&";
    }
    fullPath += paramStr;
    return fullPath;
}

/**
 * 前往跳转
 * @param path 页面路径
 * @param params 参数
 */
result.navTo = function (path, params) {
    let fullPath = this.getFullPath(path, params);
    uni.navigateTo({
        url: fullPath,
    })
}

/**
 * 关闭当前页面，跳转到应用内的某个页面
 * @param path 页面路径
 * @param params 参数
 */
result.redirectTo = function (path,params) {
    let fullPath = this.getFullPath(path, params);
    uni.redirectTo({
        url: path
    })
}

/**
 * 关闭所有页面，打开到应用内的某个页面
 * @param path 页面路径
 * @param params 参数
 */
result.reLaunch = function (path,params) {
    let fullPath = this.getFullPath(path, params);
    uni.reLaunch({
        url: fullPath
    })
}

/**
 * 前往页面
 * @param src 页面路径
 * @param data 参数
 */
result.toWeb = function (src,data) {
    if(!src){
        throw new Error("src(第1个参数)不能为空");
    }
    data = data || {
        // 传递的参数
        params: {},
        // 是否隐藏导航栏
        isHideNavBar: false,
        // 是否隐藏状态栏
        isHideStatusBar: false,
        // 标题
        title: "",
    };

    let params = data.params || {};
    for(let key in params) {
        if (params[key]) {
            src += (src.indexOf("?") === -1 ? "?" : "&") + key + "=" + encodeURIComponent(params[key]);
        }
    }
    src=encodeURIComponent(src);

    let paramStr = "";
    for(let key in data) {
        if (key !== "params") {
            paramStr += key + "=" + data[key] + "&";
        }
    }
    paramStr = paramStr.substring(0, paramStr.length - 1);

    uni.navigateTo({
        url: '/uni_modules/uniapp-kantboot/pages/web/web?src=' + src + "&" + paramStr
    });
}

export default result;