import requestSecurity from "../requestSecurity";
import storage from "../storage";
import event from "@/uni_modules/uniapp-kantboot/libs/event";

let result = {};

/**
 * 获取人才计划列表
 */
result.requestSelf = ()=> {
    // 获取用户自身信息
    return new Promise((resolve, reject) => {
        requestSecurity.send({
            // 检测到未登录，不自动跳转登录页
            isJumpLogin:false,
            uri: "/user-account-web/userAccount/getSelf",
            stateSuccess: (res) => {
                console.debug("获取用户自身信息成功", res);
                // 保存用户信息到缓存
                storage.set("userAccount:self", res.data);
                // 设置成已登录
                storage.set("userAccount:isLogin", true);
                event.emit("userAccount:getSelf", res.data);
                resolve(res);
            },
            stateFail: (res) => {
                if(res?.stateCode==="notLogin"){
                    // 删除用户信息
                    storage.remove("userAccount:self");
                    // 设置成未登录
                    storage.set("userAccount:isLogin", false);
                }
                // 如果网络错误
                if(res?.stateCode==="networkError"){
                    uni.showToast({
                        title: "网络错误",
                        icon: "none"
                    });
                }

                reject(res);
            }
        })
    });
}


/**
 * 设置自身信息
 */
result.setSelf = function(self) {
    storage.set("userAccount:self", self);
}

/**
 * 获取自身信息
 */
result.getSelf = function() {
    return storage.get("userAccount:self");
}


/**
 * 是否在登录状态
 */
result.getIsLogin = function() {
    return storage.get("userAccount:isLogin");
}

/**
 * 设置登录状态
 */
result.setIsLogin = function(isLogin) {
    storage.set("userAccount:isLogin", isLogin);
}


export default result;