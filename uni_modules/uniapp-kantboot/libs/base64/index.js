import base64ToPathService from './base64ToPathService';

let result = {};

/**
 * base64转路径
 */
result.base64ToPath = (base64) => {
    return base64ToPathService.base64ToPath(base64);
}

export default result;
