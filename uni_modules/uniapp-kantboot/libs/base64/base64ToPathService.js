import config from "@/uni_modules/uniapp-kantboot/libs/config";
import util from "@/uni_modules/uniapp-kantboot/libs/util";

let result = {};

// 安卓的文件目临时目录
let ANDROID_FILE_TEMP_DIR = config.appConfig.androidFileTempDir;

// 定义一个方法获取一个新的文件ID
let getNewFileId = () => {
    // 返回新的文件ID
    return util.generateUUID(64);
}

// 定义一个方法将base64转换为路径
result.base64ToPath =(base64)=> {
    // 返回一个新的Promise对象
    return new Promise(function(resolve, reject) {
        // 检查是否在浏览器环境中
        if (typeof window === "object" && "document" in window) {
            // 将base64字符串分割为数组
            base64 = base64.split(",");
            // 获取base64数据的类型
            let type = base64[0].match(/:(.*?);/)[1];
            // 解码base64数据
            let str = atob(base64[1]);
            // 获取解码后的字符串长度
            let n = str.length;
            // 创建一个新的Uint8Array对象
            let array = new Uint8Array(n);
            // 将解码后的字符串转换为Uint8Array对象
            while (n--) {
                array[n] = str.charCodeAt(n);
            }
            // 返回一个新的Blob对象的URL
            return resolve(
                (window.URL || window.webkitURL).createObjectURL(
                    new Blob([array], { type: type })
                )
            );
        }

        // 获取base64数据的扩展名
        let extName = base64.match(/data\:\S+\/(\S+);/);
        if (extName) {
            extName = extName[1];
        } else {
            // 如果无法获取扩展名，则抛出错误
            reject(new Error("base64 error"));
        }
        // 生成一个新的文件名
        let fileName = getNewFileId() + "." + extName;
        // 检查是否在plus环境中
        if (typeof plus === "object") {
            // 定义基础路径
            let basePath = "_doc";
            // 定义目录路径
            let dirPath = "uniapp_temp";
            // 定义文件路径
            let filePath = basePath + "/" + dirPath + "/" + fileName;
            // 检查plus的版本
            if (
                !biggerThan(
                    plus.os.name === "Android" ? "1.9.9.80627" : "1.9.9.80472",
                    plus.runtime.innerVersion
                )
            ) {
                // 解析本地文件系统URL
                plus.io.resolveLocalFileSystemURL(
                    basePath,
                    function(entry) {
                        // 获取目录
                        entry.getDirectory(
                            dirPath,
                            {
                                create: true,
                                exclusive: false,
                            },
                            function(entry) {
                                // 获取文件
                                entry.getFile(
                                    fileName,
                                    {
                                        create: true,
                                        exclusive: false,
                                    },
                                    function(entry) {
                                        // 创建写入器
                                        entry.createWriter(function(writer) {
                                            // 当写入成功时，返回文件路径
                                            writer.onwrite = function() {
                                                resolve(filePath);
                                            };
                                            // 当写入失败时，抛出错误
                                            writer.onerror = reject;
                                            // 将写入器的位置设置为文件的开始
                                            writer.seek(0);
                                            // 将base64数据写入文件
                                            writer.writeAsBinary(
                                                base64.replace(
                                                    /^data:\S+\/\S+;base64,/,
                                                    ""
                                                )
                                            );
                                        }, reject);
                                    },
                                    reject
                                );
                            },
                            reject
                        );
                    },
                    reject
                );
                return;
            }
            // 创建一个新的Bitmap对象
            let bitmap = new plus.nativeObj.Bitmap(fileName);
            // 加载base64数据
            bitmap.loadBase64Data(
                base64,
                function() {
                    // 保存Bitmap对象
                    bitmap.save(
                        filePath,
                        {},
                        function() {
                            // 清除Bitmap对象
                            bitmap.clear();
                            // 返回文件路径
                            resolve(filePath);
                        },
                        function(error) {
                            // 清除Bitmap对象
                            bitmap.clear();
                            // 抛出错误
                            reject(error);
                        }
                    );
                },
                function(error) {
                    // 清除Bitmap对象
                    bitmap.clear();
                    // 抛出错误
                    reject(error);
                }
            );
            return;
        }
        // 检查是否在微信小程序环境中
        if (typeof wx === "object" && wx.canIUse("getFileSystemManager")) {
            // 定义文件路径
            let filePath = wx.env.USER_DATA_PATH + "/" + fileName;
            // 写入文件
            wx.getFileSystemManager().writeFile({
                filePath: filePath,
                data: base64.replace(/^data:\S+\/\S+;base64,/, ""),
                encoding: "base64",
                success: function() {
                    // 返回文件路径
                    resolve(filePath);
                },
                fail: function(error) {
                    // 抛出错误
                    reject(error);
                },
            });
            return;
        }
        // 如果都不支持，抛出错误
        reject(new Error("not support"));
    });
}

// 导出result对象
export default result;