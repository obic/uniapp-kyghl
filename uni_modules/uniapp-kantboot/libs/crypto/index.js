import CryptoJS from './crypto-js';

// 加密数据
function encryptData(data, key) {
    // PKCS5Padding
    return CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(key), {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    }).toString();
}

// 解密数据
function decryptData(ciphertext, key) {
    let bytes = CryptoJS.AES.decrypt(ciphertext, CryptoJS.enc.Utf8.parse(key), {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
    });
    return bytes.toString(CryptoJS.enc.Utf8);
}

export default {
    encryptData,
    decryptData
}