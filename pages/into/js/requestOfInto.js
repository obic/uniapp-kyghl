import $kt from "@/uni_modules/uniapp-kantboot";
import shop from "@/js/shop"
import toolArea from "@/js/toolArea";

let send = async ()=> {
    let emitKey = "into:request";
    return new Promise((resolve) => {
        $kt.global.dataChange.checkDataChange("clientInit").then(async (res) => {
            if(res.isChange){

                $kt.event.emit(emitKey,"loading language pack");
                // 加载语言本地化
                await $kt.i18n.loadingLocalized();
                // 加载中文的语言
                await $kt.i18n.loadLanguage("appFront","zh_CN");
                // 加载本机的语言
                await $kt.i18n.loadLanguage("appFront");
                // 加载中文的语言
                await $kt.i18n.loadLanguage("stateArea","zh_CN");
                // 加载本机的语言
                await $kt.i18n.loadLanguage("stateArea");


                $kt.event.emit(emitKey,$kt.i18n.zhToGlobal("加载商品分类中"));
                await shop.requestI18nAll();
                await shop.requestAllHasChildren();
                // 获取所有参数
                await shop.requestParamI18nAll();
                // 获取所有属性
                await shop.requestAttrI18nAll();

                $kt.event.emit(emitKey,$kt.i18n.zhToGlobal("加载国家地区中"));
                await toolArea.requestStateArea();

                await $kt.global.dataChange.setDataChange("clientInit",res.uuid);
                resolve();
            }
            resolve();
        });
    });
}

export default {
    send
}