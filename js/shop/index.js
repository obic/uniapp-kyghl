import requestSecurity from "@/uni_modules/uniapp-kantboot/libs/requestSecurity";
import storage from "@/uni_modules/uniapp-kantboot/libs/storage";
import request from "@/uni_modules/uniapp-kantboot/libs/request";

let result = {};

/**
 * 获取商品分类（树形结构）
 */
result.requestAllHasChildren = () => {
    // 获取用户自身信息
    return new Promise((resolve, reject) => {
        request.send({
            // 检测到未登录，不自动跳转登录页
            isJumpLogin: false,
            uri: "/business-shop-web/shopGoodsType/getAllHasChildren",
            stateSuccess: (res) => {
                console.debug("获取商品分类成功", res);
                storage.set("shopGoodsType:allHasChildren", res.data);
                resolve(res);
            },
            stateFail: (res) => {
                // 如果网络错误
                if (res?.stateCode === "networkError") {
                    uni.showToast({
                        title: "网络错误",
                        icon: "none"
                    });
                }

                reject(res);
            }
        })
    });
}

/**
 * 获取商品分类（树形结构）
 */
result.getAllHasChildren = () => {
    return storage.get("shopGoodsType:allHasChildren");
}

/**
 * 获取所有商品分类的国际化非树形结构
 */
result.getI18nAll = () => {
    return storage.get("shopGoodsTypeI8n:all");
}


/**
 * 请求商品分类的国际化（树形结构）
 */
result.requestI18nAll = () => {
    return new Promise((resolve, reject) => {
        request.send({
            uri: "/business-shop-web/shopGoodsTypeI18n/getAll",
            stateSuccess: (res) => {
                console.debug("获取商品分类成功", res);
                storage.set("shopGoodsTypeI8n:all", res.data);
                let map = {};
                res.data.forEach((item) => {
                    map[item.id + ""] = item;
                });
                storage.set("shopGoodsTypeI8n:map", map);
                resolve(res);
            },
            stateFail: (res) => {
                reject(res);
            }
        })
    });
}

/**
 * 根据商品分类id获取国际化
 */
result.getI18nByGoodsTypeId = (goodsTypeId) => {
    goodsTypeId = goodsTypeId + "";
    let map = storage.get("shopGoodsTypeI8n:map");
    return map[goodsTypeId];
}

result.requestParamI18nAll = () => {
    return new Promise((resolve, reject) => {
        request.send({
            uri: "/business-shop-web/shopGoodsParamI18n/getAll",
            stateSuccess: (res) => {
                console.debug("获取商品分类参数国际化成功", res);
                storage.set("shopGoodsParamI18n:all", res.data);
                let map = {};
                res.data.forEach((item) => {
                    map[item.id + ""] = item;
                });
                storage.set("shopGoodsParamI18n:map", map);
                resolve(res);
            },
            stateFail: (res) => {
                reject(res);
            }
        })
    });
}

result.getParamI18nAll = () => {
    return storage.get("shopGoodsParamI18n:all");
}

result.getParamI18nByGoodsParamId = (goodsParamId) => {
    goodsParamId = goodsParamId + "";
    let map = storage.get("shopGoodsParamI18n:map");
    return map[goodsParamId];
}

result.requestAttrI18nAll = () => {
    return new Promise((resolve, reject) => {
        request.send({
            uri: "/business-shop-web/shopGoodsAttrI18n/getAll",
            stateSuccess: (res) => {
                console.debug("获取商品分类属性国际化成功", res);
                storage.set("shopGoodsAttrI18n:all", res.data);
                let map = {};
                res.data.forEach((item) => {
                    map[item.id + ""] = item;
                });
                storage.set("shopGoodsAttrI18n:map", map);
                resolve(res);
            },
            stateFail: (res) => {
                reject(res);
            }
        })
    });
}

result.getAttrI18nAll = () => {
    return storage.get("shopGoodsAttrI18n:all");
}

result.getAttrI18nByGoodsAttrId = (goodsAttrId) => {
    goodsAttrId = goodsAttrId + "";
    let map = storage.get("shopGoodsAttrI18n:map");
    return map[goodsAttrId];
}

/**
 * 处理查询到的数据
 */
result.handleList = (bodyData) => {
    let ids = [];
    for (let i = 0; i < bodyData.length; i++) {
        let item = bodyData[i];
        ids.push(item.id);
    }
    for (let i = 0; i < bodyData.length; i++) {
        let item = bodyData[i];
        let bodyJson = [];
        if (item.bodyJsonStr) {
            bodyJson = JSON.parse(item.bodyJsonStr);
        }
        let detailJson = [];
        if (item.detailJsonStr) {
            detailJson = JSON.parse(item.detailJsonStr);
        }
        let salesVolumeNumber = 0;
        if (item.salesVolume) {
            salesVolumeNumber = item.salesVolume.number || 0;
        }
        let salesVolumeShowNumber = 0;
        if (item.salesVolumeShow) {
            salesVolumeShowNumber = item.salesVolumeShow.number || 0;
        }
        let stockNumber = 0;
        if (item.stock) {
            stockNumber = item.stock.number || 0;
        }


        if (!item.params) {
            item.params = [];
        }
        let content = item;
        content["bodyJson"] = bodyJson;
        content["detailJson"] = detailJson;
        content["salesVolumeNumber"] = salesVolumeNumber;
        content["salesVolumeShowNumber"] = salesVolumeShowNumber;
        content["stockNumber"] = stockNumber;
        bodyData[i] = content;
    }
    return new Promise((resolve, reject) => {
        request.send({
            uri: '/business-shop-web/shopGoodsI18n/getViewList',
            data: {
                goodsIdList: ids
            },
            stateSuccess: (res) => {
                for (let i = 0; i < res.data.length; i++) {
                    let item = res.data[i];
                    let bodyJson = [];
                    if (item.bodyJsonStr) {
                        bodyJson = JSON.parse(item.bodyJsonStr);
                    }
                    let detailJson = [];
                    if (item.detailJsonStr) {
                        detailJson = JSON.parse(item.detailJsonStr);
                    }
                    let salesVolumeNumber = 0;
                    if (item.salesVolume) {
                        salesVolumeNumber = item.salesVolume.number || 0;
                    }
                    let salesVolumeShowNumber = 0;
                    if (item.salesVolumeShow) {
                        salesVolumeShowNumber = item.salesVolumeShow.number || 0;
                    }
                    let stockNumber = 0;
                    if (item.stock) {
                        stockNumber = item.stock.number || 0;
                    }


                    if (!item.params) {
                        item.params = [];
                    }
                    let content = item;
                    content["bodyJson"] = bodyJson;
                    content["detailJson"] = detailJson;
                    content["salesVolumeNumber"] = salesVolumeNumber;
                    content["salesVolumeShowNumber"] = salesVolumeShowNumber;
                    content["stockNumber"] = stockNumber;
                    bodyData[i] = content;
                }
                console.log("bodyData", bodyData);
                resolve(bodyData);
            },
            stateFail: (res) => {
                reject(res);
            }
        });

    });

}


export default result;